package com.junioroffers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JuniorOffersApplication {

    public static void main(String[] args) {
        SpringApplication.run(JuniorOffersApplication.class, args);
    }

}
